![renv diagram](renvdiagram.svg)

The diagram was generated by [drawio desktop](https://github.com/jgraph/drawio-desktop/releases)

# Create renv.lock [Dir2]

See my note at [taichimd wiki](https://wiki.taichimd.us/view/R_packages#Local_R_packages). This step will either copy the binary packages from the cache or build them from the source.

```
# mkdir renvbiotrip
setwd("Dir2")
renv::init()
# Assume we are working with R 4.0.x
local({r <- getOption("repos")
       r["CRAN"] <- "https://mran.microsoft.com/snapshot/2020-10-10/" 
       options(repos=r)
})
renv::install("~/bitbucket/renvbiotrip/dataforbiotrip_0.1.1.tar.gz")
renv::install("~/bitbucket/renvbiotrip/biotrip_0.5.1.tar.gz")
renv::settings$snapshot.type("all") 
renv::snapshot()
q()
```
The same procedure can be done in a Docker container. See [taichimd wiki](https://wiki.taichimd.us/view/R_packages#Docker).

# Testing method 1: without creating a docker image [Dir3]

```
docker run --rm -it -v $(pwd):/home/docker -w /home/docker -u docker r-base:4.0.2 R

install.packages("renv")
# I forgot the purpose of the following 2 lines; seems not needed
# dir.create("renv/local", recursive = TRUE)
# system("cp *.tar.gz renv/local")
Sys.setenv(RENV_PATHS_LOCAL = ".")
renv::restore()
# This project has not yet been activated.
# Activating this project will ensure the project library is used during restore.
# Please see `?renv::activate` for more details.
# Would you like to activate this project before restore? [Y/n]: Y
# * Project '~/' loaded. [renv 0.16.0]
# * The project library is out of sync with the lockfile.
# * Use `renv::restore()` to install packages recorded in the lockfile.
# The following package(s) will be updated:
# # CRAN ===============================
# - foreign              [0.8-80 -> 0.8-79]
# - renv                 [0.16.0 -> 0.12.0]
# ...
# # (Unknown Source) ===================
# - biotrip              [* -> 0.5.1]
# - dataforbiotrip       [* -> 0.1.1]
# Do you want to proceed? [y/N]: y
# 17 min to build from source Pentium J5005
source('test.R')
# [1] 0.2065939 0.1936423
# 20 seconds on i5-8500t
q()

rm -rf renv
rm -rf .cache
rm .Rprofile
```

PS

* This purpose of this method is to see the effect of ``renv::restore()``. It is not meant to be used in practice.
* Note that I specify ``-u docker`` in ``docker run`` so the new files/folders belong to the current user instead of root.
* I mount the current folder in the container in order to get an access of ``test.R`` file in the container. But the side effect of this after calling ``renv::restore()`` is it will create the ``.Rprofile`` file and the ``renv`` directory. The ``renv`` directory contains 1. "activate.R" file, 2. "library" directory, 3. 'sandbox' directory, and 4. 'staging' directory.
* If I delete the ``renv`` folder but forgot to delete ``.Rprofile``, I'll get the following message when I try to start a container (see Testing method 2)
```
Error in file(filename, "r", encoding = encoding) :
  cannot open the connection
In addition: Warning message:
In file(filename, "r", encoding = encoding) :
  cannot open file 'renv/activate.R': No such file or directory
```

# Testing method 2: creating a docker image [Dir4]

PS.

* This assume the directory does not contain ".Rprofile" that was created by the method 1.
* The advantage of this approach is the required R packages are already in the container.

## Create a docker image

```
# With a support to generate PDF/HTML files
docker build -t renvbiotrip .
# No support to generate PDF/HTML files
docker build -t renvbiotripbase -f Dockerfile_r-base .
```

## User Docker container to run an analysis

```
# Dockerfile based on r-base
docker run --rm -it -v $(pwd):/report -w /report renvbiotripbase
time docker run --rm -v $(pwd):/report -w /report renvbiotripbase \
            Rscript -e "source('test.R')"   # 40sec on Pentium J5005

# Dockerfile based on rocker/verse which includes tex tool
docker run --rm -it -v $(pwd):/report -w /report renvbiotrip
time docker run --rm -v $(pwd):/report -w /report renvbiotrip \
            Rscript -e "rmarkdown::render('test.Rmd')"     # 8min 30sec on Pentium J5005, 4min on i5-8500t

time docker run --rm -v $(pwd):/report -w /report renvbiotrip \
            Rscript -e "rmarkdown::render('testHTML.Rmd')" # 22sec on i5-8500t
```

# Other enhancement
Check out [How to compile R Markdown documents using Docker](https://jlintusaari.github.io/2018/07/how-to-compile-rmarkdown-documents-using-docker/). I can use their Dockerfile as a base and modify it for my need so I can use docker to create a report. The goal is I only need to supply an Rmd file in order to create a PDF report. The Rmd file will install the necessary packages if they are not present in the image.
