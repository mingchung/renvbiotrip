FROM rocker/verse:4.0.2

RUN R -e 'install.packages("renv")'
RUN R -e 'install.packages("rmarkdown")'
RUN mkdir -p /home/rstudio/renv/local
COPY renv.lock /home/rstudio
COPY biotrip_0.5.1.tar.gz /home/rstudio/renv/local
COPY dataforbiotrip_0.1.1.tar.gz /home/rstudio/renv/local
WORKDIR /home/rstudio
RUN R -e 'renv::restore()'
USER rstudio
CMD ["R"]
