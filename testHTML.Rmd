---
title: "Generate HTML report from Docker"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    code_folding: hide
    toc: true
    toc_float: true
    theme: "flatly"
editor_options:
  chunk_output_type: console
---


```{r}
library(biotrip)
library(dataforbiotrip)
data("breastcancer_gse6532")
data.1 <- breastcancer_gse6532$data.1[1:5000,]
pheno.1 <- breastcancer_gse6532$pheno.1
data.2 <- breastcancer_gse6532$data.2[1:5000,]
pheno.2 <- breastcancer_gse6532$pheno.2
data.train <- data.frame(time = pheno.1$SurvTime,
                        status = pheno.1$SurvStatus,
                        treat = pheno.1$Treatment,
                         t(data.1))
data.new <- data.frame(time = pheno.2$SurvTime,
                      status = pheno.2$SurvStatus,
                      treat = pheno.2$Treatment,
                      t(data.2))
set.seed(1234)
res <- modCov(data = data.train, x = 4:ncol(data.train), y = c(1,2),
              tt = 3, folds = 10)
dc <- deltaC(res, data.train, data.new, x = 4:ncol(data.train), time = c(3, 5)*365)
print(dc)
```
